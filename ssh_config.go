package main

import (
	"fmt"
	"strings"
)

type UserConfig struct {
	Path       string
	PGPKeyPath string
	SSHRoot    string
}

func NewUserConfig(path string) *UserConfig {
	return &UserConfig{Path: path}
}

func (uc UserConfig) SetPGPPath(path string) {
	uc.PGPKeyPath = path
}

type SSHConfig struct {
	Options []Option `json:"options"`
	Hosts   []Host   `json:"hosts",omitempty`
}

func (sc SSHConfig) Serialize() string {
	var str strings.Builder

	for _, host := range sc.Hosts {
		str.WriteString(host.Serialize())
	}

	return str.String()
}

type Option struct {
	Name      string   `json:"name"`
	Value     string   `json:"value"`
	Platforms []string `json:"platforms",omitempty`
}

func (o Option) Serialize() string {
	return fmt.Sprintf("%s %s", o.Name, o.Value)
}
