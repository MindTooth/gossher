package main

import (
	"fmt"
	"strings"
)

type Host struct {
	Host         string `json:"host"`
	HostName     string `json:"hostname"`
	IdentityFile string `json:"identity_file"`
	User         string `json:"user"`
	ForwardAgent bool   `json:"forward_aget"`
	ProxyCommand string `json:"proxy_command"`
	ProxyJump    string `json:"proxy_jump"`
	Port         uint8  `json:"port"`
}

func (h *Host) Serialize() string {
	var str strings.Builder

	str.WriteString(fmt.Sprintf("Host %s\n", h.Host))

	if h.HostName != "" {
		str.WriteString(fmt.Sprintf("HostName %s\n", h.HostName))
	}

	if h.HostName != "" {
		str.WriteString(fmt.Sprintf("IdentityFile %s\n", h.IdentityFile))
	}

	return str.String()
}
