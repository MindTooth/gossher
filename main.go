package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	rawJson := "ssh_config.json"
	js, err := os.Open(rawJson)

	if err != nil {
		panic(err)
	}

	defer func() {
		if err = js.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	r, _ := ioutil.ReadAll(js)

	var ssh_config SSHConfig
	json.Unmarshal(r, &ssh_config)

	fmt.Println("# Global Options")

	for _, option := range ssh_config.Options {
		fmt.Println(option.Serialize())
	}

	fmt.Println("\n# Hosts")

	for _, host := range ssh_config.Hosts {
		fmt.Println(host.Serialize())
	}
}
